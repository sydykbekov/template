export const RU_ru = {
  translation: {
    login: {
      title: 'Авторизация',
      inputLogin: 'Логин',
      inputPassword: 'Пароль',
      button: 'Войти',
      successMsg: 'Авторизация прошла удачно',
      errorMsg: 'Ошибка авторизации',
    },

    layout: {
      catalogManagement: 'Управление каталогами',
      docTypeManagement: 'Управление типами документов',
      workWithDocs: 'Работа с документами',
      search: 'Поиск'
    },

    catalogManagement: {
      button: 'Новый каталог',
      title: 'Управление каталогами',
      edit: 'редактировать',
      delete: 'удалить',
      start: 'в начало',
      previous: 'предыдущий',
      next: 'следующий',
      end: 'в конец',
      searchBoxPlaceholder: 'Поиск каталога',
      fetchNumberFailed: 'Не удалось получить количество каталогов',
      fetchFailed: 'Не удалось получить каталоги',
      deletedSuccess: 'Каталог удален!',
      deletedFailure: 'Не удалось удалить каталог',
      confirm: 'Подтвердите действие',
      deleteConfirm: 'Удалить',
      cancelConfirm: 'Отменить',
    },

    newCatalog: {
      title: 'Управление каталогами / Новый каталог',
      kkName: 'Наименование на казахском',
      ruName: 'Наименование на русском',
      enName: 'Наименование на английском',
      kkDescription: 'Описание на казахском',
      ruDescription: 'Описание на русском',
      enDescription: 'Описание на английском',
      create: 'Создать',
      successMessage: 'Новый каталог создан',
      errorMessage: 'Не удалось создать новый каталог'
    },

    editCatalog: {
      title: 'Редактировать каталог',
      errorMessage: 'Не удалось изменить каталог',
      saveChanges: 'Сохранить изменения',
      successMessage: 'Изменения сохранены',
      cantSave: 'Не удалось сохранить изменения',
    },

    viewCatalog: {
      title: 'Просмотр каталога'
    },

    documentTypeManagement: {
      button: 'Новый тип документа',
      title: 'Управление типами документов',
      edit: 'редактировать',
      delete: 'удалить',
      start: 'в начало',
      previous: 'предыдущий',
      next: 'следующий',
      end: 'в конец',
      searchBoxPlaceholder: 'Поиск типа документа',
      fetchNumberFailed: 'Не удалось получить количество типов документов',
      fetchFailed: 'Не удалось получить типы документов',
      deletedSuccess: 'Тип документа удален!',
      deletedFailure: 'Не удалось удалить тип документа',
      confirm: 'Подтвердите действие',
      deleteConfirm: 'Удалить',
      cancelConfirm: 'Отменить',
    },

    newDocType: {
      title: 'Управление типами документов / Новый тип документа',
      kkName: 'Наименование на казахском',
      ruName: 'Наименование на русском',
      enName: 'Наименование на английском',
      kkDescription: 'Описание на казахском',
      ruDescription: 'Описание на русском',
      enDescription: 'Описание на английском',
      create: 'Создать',
      metaTitle: 'Метаданные типа',
      addMeta: 'Добавить секцию метаданных',
      code: 'Код поля',
      type: 'Тип поля',
      required: 'Обязательность поля',
      fillTypes: 'Выберите тип для всех метаданных',
      successMessage: 'Новый тип добавлен',
      errorMessage: 'Не удалось добавить тип',
      recurrent: 'Массив полей'
    },

    workWithDocs: {
      title: 'Работа с документами',
      button: 'Новый документ',
      searchByName: 'Поиск по имени файла',
      catalog: 'Каталог',
      docType: 'Тип документа',
      noMessage: 'Ничего не найдено',
      colFilename: 'Имя файла',
      colDateVer: 'Дата версии',
      colCatalog: 'Каталог',
      colDocType: 'Тип документа',
      edit: 'редактировать',
      delete: 'удалить',
      successDeleted: 'Удалено'
    },

    newDoc: {
      title: 'Работа с документами / Новый документ',
      docType: 'Поиск и выбор типа документа',
      edc: 'ЭЦП',
      upload: 'Загрузить документ',
      metaTitle: 'Метаданные документа',
      submit: 'Создать',
      chooseTypeError: 'Выберите тип документа',
      chooseCatalogError: 'Выберите каталог',
      successMessage: 'Документ добавлен',
      errorMessage: 'Не удалось добавить документ',
      catalog: 'Поиск и выбор каталога',
    },

    viewDoc: {
      title: 'Работа с документами / Просмотр документа',
      catalog: 'Каталог',
      docType: 'Тип документа',
      docVersion: 'Версия документа',
      operator: 'Оператор',
      edc: 'ЭЦП',
      uploadedFiles: 'Загруженные файлы',
      editButton: 'Редактировать',
      deleteButton: 'Удалить',
      versionHistory: 'История версий',
      colFilename: 'Имя файла',
      colDateVer: 'Дата версии',
      colNumVer: 'Номер версии',
      colOperator: 'Оператор'
    },

    search: {
      title: 'Поиск',
      catalog: 'Каталог',
      docType: 'Тип документа',
      filename: 'Имя файла',
      dateFrom: 'Дата начала',
      dateTo: 'Дата конца',
      find: 'Найти',
      metaTitle: 'Метаданные документа',
      exactCheckbox: 'Полное совпадение',
      resultsTitle: 'Результаты поиска',
      findFailed: 'Не удалось отправить данные поиска',
      noMessage: 'Ничего не найдено',
      requiredData: 'Каталог и тип документа должны быть заданы',
      colFilename: 'Имя файла',
      colCatalog: 'Каталог',
      colDocType: 'Тип документа',
      edit: 'редактировать',
      delete: 'удалить',
    },

    editDocType: {
      title: 'Редактировать тип документа',
      errorMessage: 'Не удалось загрузить тип документа',
      saveChanges: 'Сохранить изменения',
      successMessage: 'Изменения сохранены',
      cantSave: 'Не удалось сохранить изменения'
    },

    viewDocType: {
      title: 'Просмотр типа документа'
    },

    editDoc: {
      subscribe: 'Подписать',
      saveChanges: 'Сохранить изменения',
      title: 'Редактировать документ',
      ncalayerError: 'Программа NCALayer не запущена',
      subscribeCanceled: 'Подпись документа отменена',
      cantGetXml: 'Не удалось получить XML'
    }
  }
};
