export const KZ_kz = {
  translation: {
    login: {
      title: 'Авторизация',
      inputLogin: 'Кіру',
      inputPassword: 'Құпия сөз',
      button: 'Жүйеге кіріңіз',
      successMsg: 'Сәтті кіру',
      errorMsg: 'Авторизация қатесі',
    },

    layout: {
      catalogManagement: 'Каталогты басқару',
      docTypeManagement: 'Құжат түрін басқару',
      workWithDocs: 'Құжаттармен жұмыс істеу',
      search: 'Іздеу'
    },

    catalogManagement: {
      button: 'Жаңа каталог',
      title: 'Каталогты басқару',
      edit: 'өңдеу',
      delete: 'жою',
      start: 'басына',
      previous: 'алдыңғы',
      next: 'келесі',
      end: 'соңында',
      searchBoxPlaceholder: 'Каталогты іздеу',
      fetchNumberFailed: 'Каталогтар санын алу мүмкін болмады',
      fetchFailed: 'Каталогтар алынбады',
      deletedSuccess: 'Каталог жойылды!',
      deletedFailure: 'Каталогты жою сәтсіз аяқталды',
      confirm: 'Әрекетті растау',
      deleteConfirm: 'Жою',
      cancelConfirm: 'Болдырмау',
    },

    newCatalog: {
      title: 'Каталогты басқару / Жаңа каталог',
      kkName: 'Қазақша атауы',
      ruName: 'Орысша атауы',
      enName: 'Ағылшын тілі атауы',
      kkDescription: 'Сипаттама қазақ тілінде',
      ruDescription: 'Сипаттама орыс тілінде',
      enDescription: 'Сипаттама ағылшын тілінде',
      create: 'Жасау',
      successMessage: 'Жаңа каталог құрылды',
      errorMessage: 'Жаңа каталог жасалмады'
    },

    editCatalog: {
      title: 'Құжат түрін өңдеу',
      errorMessage: 'Каталогты өзгерту мүмкін емес',
      saveChanges: 'Өзгерістерді сақтаңыз',
      successMessage: 'Өзгерістер сақталды',
      cantSave: 'Өзгерістерді сақтау сәтсіз аяқталды',
    },

    viewCatalog: {
      title: 'Каталогты қарау'
    },

    documentTypeManagement: {
      button: 'Жаңа құжат түрі',
      title: 'Құжат түрін басқару',
      edit: 'өңдеу',
      delete: 'жою',
      start: 'басына',
      previous: 'алдыңғы',
      next: 'келесі',
      end: 'соңында',
      searchBoxPlaceholder: 'Құжат түрін іздеу',
      fetchNumberFailed: 'Құжат түрлерінің санын алу мүмкін болмады',
      fetchFailed: 'Құжат түрлерін алу мүмкін болмады',
      deletedSuccess: 'Құжат түрі жойылды!',
      deletedFailure: 'Құжат түрін жою сәтсіз аяқталды',
      confirm: 'Әрекетті растау',
      deleteConfirm: 'Жою',
      cancelConfirm: 'Болдырмау'
    },

    newDocType: {
      title: 'Құжат түрін басқару / Жаңа құжат түрі',
      kkName: 'Қазақша атауы',
      ruName: 'Орысша атауы',
      enName: 'Ағылшын тілі атауы',
      kkDescription: 'Сипаттама қазақ тілінде',
      ruDescription: 'Сипаттама орыс тілінде',
      enDescription: 'Сипаттама ағылшын тілінде',
      create: 'Жасау',
      metaTitle: 'Метадеректерді теріңіз',
      addMeta: 'Метадеректер бөлімін қосыңыз',
      code: 'Өріс коды',
      type: 'Өріс түрі',
      required: 'Міндетті өріс',
      fillTypes: 'Барлық метадеректердің түрін таңдаңыз',
      successMessage: 'Жаңа түрі қосылды',
      errorMessage: 'Жаңа түрі қосылмады',
      recurrent: 'Өрістер массасы'
    },

    workWithDocs: {
      title: 'Құжаттармен жұмыс істеу',
      button: 'Жаңа құжат',
      searchByName: 'Файл атауы бойынша іздеу',
      catalog: 'Каталогы',
      docType: 'Құжат түрі',
      noMessage: 'Еш нәрсе табылмады',
      colFilename: 'Файл атауы',
      colDateVer: 'Версия күні',
      colCatalog: 'Каталогы',
      colDocType: 'Құжат түрі',
      edit: 'өңдеу',
      delete: 'жою',
      successDeleted: 'Жойылған'
    },

    newDoc: {
      title: 'Құжаттармен жұмыс істеу / Жаңа құжат',
      docType: 'Құжат түрін іздеп, таңдаңыз',
      edc: 'ЭСК',
      upload: 'Құжатты жүктеп алыңыз',
      metaTitle: 'Құжаттың метадеректері',
      submit: 'Жасау',
      chooseTypeError: 'Құжат түрін таңдаңыз',
      chooseCatalogError: 'Каталогты таңдаңыз',
      successMessage: 'Құжат қосылды',
      errorMessage: 'Құжатты қосу сәтсіз аяқталды',
      catalog: 'Каталогты іздеңіз және таңдаңыз'
    },

    viewDoc: {
      title: 'Құжаттармен жұмыс / Құжатты қарау',
      catalog: 'Каталогы',
      docType: 'Құжат түрі',
      docVersion: 'Құжат нұсқасы',
      operator: 'Оператор',
      edc: 'ЭСК',
      uploadedFiles: 'Жүктелген файлдар',
      editButton: 'Өңдеу үшін',
      deleteButton: 'Жою',
      versionHistory: 'Нұсқалар тарихы',
      colFilename: 'Файл атауы',
      colDateVer: 'Версия күні',
      colNumVer: 'Нұсқа нөмірі',
      colOperator: 'Оператор'
    },

    search: {
      title: 'Іздеу',
      catalog: 'Каталогы',
      docType: 'Құжат түрі',
      filename: 'Файл атауы',
      dateFrom: 'Басталу күні',
      dateTo: 'Аяқталу күні',
      find: 'Табу үшін',
      metaTitle: 'Құжаттың метадеректері',
      resultsTitle: 'Іздеу нәтижесі',
      exactCheckbox: 'Толық матч',
      findFailed: 'Іздеу деректерін жіберу сәтсіз аяқталды',
      noMessage: 'Еш нәрсе табылмады',
      requiredData: 'Каталог және құжат түрін көрсету керек',
      colFilename: 'Файл атауы',
      colCatalog: 'Каталогы',
      colDocType: 'Құжат түрі',
      edit: 'өңдеу',
      delete: 'жою',
    },

    editDocType: {
      title: 'Құжат түрін өңдеу',
      errorMessage: 'Құжат түрін жүктеу мүмкін болмады',
      saveChanges: 'Өзгерістерді сақтаңыз',
      successMessage: 'Өзгерістер сақталды',
      cantSave: 'Өзгерістерді сақтау сәтсіз аяқталды'
    },

    viewDocType: {
      title: 'Құжатты қарау құжатта'
    },

    editDoc: {
      subscribe: 'Қол қою',
      saveChanges: 'Өзгерістерді сақтаңыз',
      title: 'Құжатты өңдеңіз',
      ncalayerError: 'NCALayer іске қосылмаған',
      subscribeCanceled: 'Қол қою тоқтатылды',
      cantGetXml: 'XML-ні алып тастаған жоқ'
    }
  }
};
