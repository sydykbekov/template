import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import CatalogManagement from './containers/CatalogManagement/CatalogManagement';
import NewCatalog from './containers/NewCatalog/NewCatalog';
import DocTypeManagement from "./containers/DocTypeManagement/DocTypeManagement";
import NewDocumentsType from "./containers/NewDocumentsType/NewDocumentsType";
import WorkWithDocuments from "./containers/WorkWithDocuments/WorkWithDocuments";
import NewDocument from "./containers/NewDocument/NewDocument";
import ViewDocument from "./containers/ViewDocument/ViewDocument";
import Search from './containers/Search/Search';
import EditCatalog from "./containers/EditCatalog/EditCatalog";
import EditDocType from "./containers/EditDocType/EditDocType";
import ViewCatalog from "./containers/ViewCatalog/ViewCatalog";
import ViewDocumentType from "./containers/ViewDocumentType/ViewDocumentType";
import Login from "./containers/Login/Login";
import EditDocument from "./containers/EditDocument/EditDocument";

/*
const ProtectedRoute = ({isAllowed, ...props}) => (
  isAllowed ? <Route {...props} /> : <Redirect to="/login" />
);
*/

const Routes = ({user}) => (
  <Switch>
    <Route path="/" exact render={() => <Redirect to="/catalogs"/>}/>
    <Route path="/catalogs" exact component={CatalogManagement}/>
    <Route path="/catalogs/edit/:id" exact component={EditCatalog}/>
    <Route path="/catalogs/new_catalog" exact component={NewCatalog}/>
    <Route path="/view_catalog/:id" exact component={ViewCatalog}/>
    <Route path="/documents_type" exact component={DocTypeManagement}/>
    <Route path="/documents_type/edit/:id" exact component={EditDocType}/>
    <Route path="/documents_type/new_document_type" exact component={NewDocumentsType}/>
    <Route path="/documents_type/view_document_type/:id" exact component={ViewDocumentType}/>
    <Route path="/documents_list" exact component={WorkWithDocuments}/>
    <Route path="/documents_list/new_document" exact component={NewDocument}/>
    <Route path="/documents_list/:dirID/type/:typeID/view_document/:id" exact component={ViewDocument}/>
    <Route path="/documents_list/:dirID/type/:typeID/edit_document/:id" exact component={EditDocument}/>
    <Route path="/search" exact component={Search}/>
    <Route path="/login" exact component={Login}/>
    {/*<ProtectedRoute
      path="/products/new"
      exact
      component={NewProduct}
      isAllowed={user && user.role === 'admin'}
    />*/}
  </Switch>
);

export default Routes;
