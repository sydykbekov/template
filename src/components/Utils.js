
export
function maybeParseJSONString(value) {
  if (typeof value === 'string') {
    return JSON.parse(value);
  } else {
    return value;
  }
}
