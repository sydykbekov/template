import React, {Component} from 'react';
import Layout from "./components/Layout/Layout";
import Routes from "./Routes";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import {setLanguage} from "./store/actions/localization";


class App extends Component {
  componentWillMount() {
    this.props.setLanguage('RU_ru');
  }

  render() {
    return (
      <Layout setLanguage={this.props.setLanguage} lang={this.props.lang}>
        <Routes user={true}/>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  lang: state.localization.language.language
});

const mapDispatchToProps = dispatch => ({
  setLanguage: language => dispatch(setLanguage(language))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
