import axios from '../../axios-api';
import {NotificationManager} from 'react-notifications';
import {
  ADD_DOCUMENT, ADD_DOCUMENT_FAILURE, ADD_DOCUMENT_SUCCESS, DELETE_DOCUMENT_FAILURE, DELETE_DOCUMENT_START,
  DELETE_DOCUMENT_SUCCESS, FETCH_DOCUMENT_FAILURE, FETCH_DOCUMENT_START, FETCH_DOCUMENT_SUCCESS,
  SEARCH_DOCUMENTS_FAILURE,
  SEARCH_DOCUMENTS_START,
  SEARCH_DOCUMENTS_SUCCESS
} from "../actionTypes";
import {translate} from "../../localization/i18n";

function addDocument() {
  return {type: ADD_DOCUMENT};
}

function addDocumentSuccess() {
  return {type: ADD_DOCUMENT_SUCCESS};
}

function addDocumentFailure(error) {
  return {type: ADD_DOCUMENT_FAILURE, error};
}

export function addDoc(formData, directoryID) {
  return dispatch => {
    dispatch(addDocument());
    console.log(formData);
    axios.post(`/directory/${directoryID}/document`, formData).then(response => {
      if (response.data.status === 1) {
        dispatch(addDocumentSuccess());
        NotificationManager.success(translate('newDoc.successMessage'));
      } else {
        dispatch(addDocumentFailure(response.data));
        NotificationManager.error(translate('newDoc.errorMessage'));
      }
    }, error => {
      dispatch(addDocumentFailure(error));
      NotificationManager.error(translate('newDoc.errorMessage'));
    })
  }
}

function searchDocumentsStart() {
  return {type: SEARCH_DOCUMENTS_START};
}

function searchDocumentsSuccess(foundDocs) {
  return {type: SEARCH_DOCUMENTS_SUCCESS, foundDocs};
}

function searchDocumentsFailure(error) {
  return {type: SEARCH_DOCUMENTS_FAILURE, error};
}

export function searchDocs(data) {
  return dispatch => {
    dispatch(searchDocumentsStart());
    axios.post('/document/search', data).then(response => {
      if (response.data.status === 1) {
        dispatch(searchDocumentsSuccess(response.data.object));
      } else {
        dispatch(searchDocumentsFailure(response.data));
      }
    }, error => {
      console.log(error);
      dispatch(searchDocumentsFailure(error));
    })
  }
}

function deleteDocumentStart() {
  return {type: DELETE_DOCUMENT_START};
}

function deleteDocumentSuccess(id) {
  return {type: DELETE_DOCUMENT_SUCCESS, id};
}

function deleteDocumentFailure() {
  return {type: DELETE_DOCUMENT_FAILURE};
}

export function deleteDocument(dirID, docID) {
  return dispatch => {
    dispatch(deleteDocumentStart());
    axios.delete(`/directory/${dirID}/document/${docID}`).then(response => {
      if (response.data.status === 1) {
        NotificationManager.success(translate('workWithDocs.successDeleted'));
        dispatch(deleteDocumentSuccess(response.data.object.id));
      }
    }, error => {
      console.log(error);
      dispatch(deleteDocumentFailure());
    })
  }
}

function fetchDocumentStart() {
  return {type: FETCH_DOCUMENT_START};
}

function fetchDocumentSuccess(document) {
  return {type: FETCH_DOCUMENT_SUCCESS, document};
}

function fetchDocumentFailure(error) {
  return {type: FETCH_DOCUMENT_FAILURE, error};
}

export function fetchDocument(dirID, docID) {
  return dispatch => {
    dispatch(fetchDocumentStart());
    axios.get(`/directory/${dirID}/document/${docID}?getBinaryData=true`).then(response => {
      if (response.data.status === 1) {
        dispatch(fetchDocumentSuccess(response.data.object));
      } else {
        dispatch(fetchDocumentFailure(response.data));
      }
    }, error => {
      dispatch(fetchDocumentFailure(error));
    })
  }
}