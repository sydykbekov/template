import axios from '../../axios-api';
import {NotificationManager} from 'react-notifications';
import {translate} from "../../localization/i18n";

import {
  AUTH_LOGIN, AUTH_LOGIN_SUCCESS, AUTH_LOGIN_FAILURE
} from "../actionTypes";

function authLogin_() {
  return {type: AUTH_LOGIN};
}

function authLoginSuccess_(authInfo) {
  return {type: AUTH_LOGIN_SUCCESS, authInfo};
}

function authLoginFailure_(error) {
  return {type: AUTH_LOGIN_FAILURE, error};
}

export function authLogin(credentials) {
  return dispatch => {
    dispatch(authLogin_());

    axios.post(`/login`, credentials)
      .then(response => {
          if (response.data.status === "SUCCESS") {
            NotificationManager.success(translate('login.successMsg'));
            dispatch(authLoginSuccess_(response.data));
          } else {
            NotificationManager.error(`${translate('login.errorMsg')}: ${response.data}`);
            dispatch(authLoginFailure_(response.data));
          }
        },
        error => {
          NotificationManager.error(`${translate('login.errorMsg')}: ${error}`);
          dispatch(authLoginFailure_(error));
        });
  };
}
