import {NotificationManager} from 'react-notifications';
import axios from '../../axios-api';
import {translate} from "../../localization/i18n";
import {
  SEARCH_BY_WORD, SEARCH_BY_WORD_FAILURE, SEARCH_BY_WORD_SUCCESS,
  SEARCH_CATALOGS, SEARCH_CATALOGS_SUCCESS, SEARCH_CATALOGS_FAILURE,
  SEARCH_DOCUMENT_TYPES, SEARCH_DOCUMENT_TYPES_SUCCESS, SEARCH_DOCUMENT_TYPES_FAILURE,
  SEARCH_DOCS, SEARCH_DOCS_SUCCESS, SEARCH_DOCS_FAILURE, RESET_SEARCH_DOCS,
} from "../actionTypes";

const searchRequestHelper = (urlFmt, onInit, onSuccess, onError) => dispatch => {
  dispatch(onInit());

  axios.get(urlFmt())
    .then(response => {
      if (response.data.status === 1) {
        dispatch(onSuccess(response.data.object));
      } else {
        dispatch(onError(response.data));
      }
    }, error => {
      dispatch(onError(error));
    });
};

function searchByWord() {
  return {type: SEARCH_BY_WORD};
}

function searchByWordSuccess(result) {
  return {type: SEARCH_BY_WORD_SUCCESS, result};
}

function searchByWordFailure(error) {
  return {type: SEARCH_BY_WORD_FAILURE, error};
}

export const searchCatalog = word =>
  searchRequestHelper(
    () => `/directory/find?word=${word}`,
    searchByWord,
    searchByWordSuccess,
    searchByWordFailure,
  );

export const searchDocType = word =>
  searchRequestHelper(
    () => `/documentType/find?word=${word}`,
    searchByWord,
    searchByWordSuccess,
    searchByWordFailure,
  );

function searchCatalogs_() {
  return {type: SEARCH_CATALOGS};
}

function searchCatalogsSuccess_(foundCatalogs) {
  return {type: SEARCH_CATALOGS_SUCCESS, foundCatalogs};
}

function searchCatalogsFailure_(error) {
  return {type: SEARCH_CATALOGS_FAILURE, error};
}

export const searchCatalogs = word =>
  searchRequestHelper(
    () => `/directory/find?word=${word}`,
    searchCatalogs_,
    searchCatalogsSuccess_,
    searchCatalogsFailure_,
  );

function searchDocTypes_() {
  return {type: SEARCH_DOCUMENT_TYPES};
}

function searchDocTypesSuccess_(foundDocTypes) {
  return {type: SEARCH_DOCUMENT_TYPES_SUCCESS, foundDocTypes};
}

function searchDocTypesFailure_(error) {
  return {type: SEARCH_DOCUMENT_TYPES_FAILURE, error};
}

export const searchDocTypes = word =>
  searchRequestHelper(
    () => `/documentType/find?word=${word}`,
    searchDocTypes_,
    searchDocTypesSuccess_,
    searchDocTypesFailure_,
  );

const searchDocuments_ = () =>
  ({type: SEARCH_DOCS});

const searchDocumentsSuccess_ = foundDocs =>
  ({type: SEARCH_DOCS_SUCCESS, foundDocs});

const searchDocumentsFailure_ = error =>
  ({type: SEARCH_DOCS_FAILURE, error});

export const searchDocuments = data => dispatch => {
  dispatch(searchDocuments_());

  axios.post('/document/search', data)
    .then(response => {
        if (response.data.status === 1) {
          dispatch(searchDocumentsSuccess_(response.data.object));
        } else {
          NotificationManager.info(response.data.message || translate('search.noMessage'));
          dispatch(searchDocumentsFailure_(response.data));
        }
      },
      error => {
        NotificationManager.error(translate('search.findFailed'));
        dispatch(searchDocumentsFailure_(error));
      });
};

const resetSearchDocuments_ = () =>
  ({type: RESET_SEARCH_DOCS});

export const resetSearchDocuments = () => dispatch => {
  dispatch(resetSearchDocuments_());
};
