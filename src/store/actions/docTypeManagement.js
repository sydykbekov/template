import axios from '../../axios-api';
import {NotificationManager} from 'react-notifications';
import {translate} from "../../localization/i18n";
import {maybeParseJSONString} from "../../components/Utils"

import {
  ADD_DOCUMENT_TYPE, ADD_DOCUMENT_TYPE_FAILURE, ADD_DOCUMENT_TYPE_SUCCESS, CHANGE_DOCUMENT_TYPE,
  CHANGE_DOCUMENT_TYPE_FAILURE,
  CHANGE_DOCUMENT_TYPE_SUCCESS, DELETE_DOCUMENT_TYPE,
  DELETE_DOCUMENT_TYPE_FAILURE,
  DELETE_DOCUMENT_TYPE_SUCCESS, FETCH_DOCUMENT_TYPE, FETCH_DOCUMENT_TYPE_FAILURE, FETCH_DOCUMENT_TYPE_SUCCESS,
  FETCH_DOCUMENT_TYPES,
  FETCH_DOCUMENT_TYPES_SUCCESS,
  FETCH_DOCUMENTS_TYPE_FAILURE,
  FETCH_DOCUMENTS_TYPES_NUMBER, FETCH_DOCUMENTS_TYPES_NUMBER_SUCCESS, FETCH_DOCUMENTS_TYPES_NUMBER_FAILURE
} from "../actionTypes";

function fetchDocumentTypesNumber() {
  return {type: FETCH_DOCUMENTS_TYPES_NUMBER}
}

function fetchDocumentTypesNumberSuccess(docTypesNumber) {
  return {type: FETCH_DOCUMENTS_TYPES_NUMBER_SUCCESS, docTypesNumber};
}

function fetchDocumentTypesNumberFailure(error) {
  return {type: FETCH_DOCUMENTS_TYPES_NUMBER_FAILURE, error};
}

export function fetchDocTypesNumber() {
  return dispatch => {
    dispatch(fetchDocumentTypesNumber());
    axios.get('documentType/pagesCount')
      .then(response => {
        if (response.data.status === 1) {
          dispatch(fetchDocumentTypesNumberSuccess(maybeParseJSONString(response.data.object)));
        } else {
          dispatch(fetchDocumentTypesNumberFailure(response.data));
        }
      }, error => {
        NotificationManager.error(translate('documentTypeManagement.fetchNumberFailed'));
        dispatch(fetchDocumentTypesNumberFailure(error));
      })
  }
}

function fetchDocumentTypes() {
  return {type: FETCH_DOCUMENT_TYPES}
}

function fetchDocumentTypesSuccess(types) {
  return {type: FETCH_DOCUMENT_TYPES_SUCCESS, types};
}

function fetchDocumentTypesFailure(error) {
  return {type: FETCH_DOCUMENTS_TYPE_FAILURE, error};
}

export function fetchDocTypes(page) {
  return dispatch => {
    dispatch(fetchDocumentTypes());

    axios.get(`documentType/?page=${page}`).then(response => {
      if (response.data.status === 1) {
        dispatch(fetchDocumentTypesSuccess(response.data.object));
      } else {
        dispatch(fetchDocumentTypesFailure(response.data));
      }
    }, error => {
      NotificationManager.error(translate('documentTypeManagement.fetchFailed'));
      dispatch(fetchDocumentTypesFailure(error));
    })
  }
}

function addDocumentType() {
  return {type: ADD_DOCUMENT_TYPE};
}

function addDocumentTypeSuccess() {
  return {type: ADD_DOCUMENT_TYPE_SUCCESS};
}

function addDocumentTypeFailure(error) {
  return {type: ADD_DOCUMENT_TYPE_FAILURE, error};
}

export function addDocType(data) {
  return dispatch => {
    dispatch(addDocumentType());
    axios.post('/documentType', data).then(response => {
      if (response.data.status === 1) {
        NotificationManager.success(translate('newDocType.successMessage'));
        return dispatch(addDocumentTypeSuccess());
      } else {
        NotificationManager.error(translate('newDocType.errorMessage'));
        return dispatch(addDocumentTypeFailure(response.data));
      }
    }, error => {
      NotificationManager.error(translate('newDocType.errorMessage'));
      dispatch(addDocumentTypeFailure(error));
    })
  }
}

function deleteDocumentType() {
  return {type: DELETE_DOCUMENT_TYPE};
}

function deleteDocumentTypeSuccess() {
  return {type: DELETE_DOCUMENT_TYPE_SUCCESS};
}

function deleteDocumentTypeFailure(error) {
  return {type: DELETE_DOCUMENT_TYPE_FAILURE, error};
}

export function deleteDocType(id, page) {
  return dispatch => {
    dispatch(deleteDocumentType());
    axios.delete(`/documentType/${id}`).then(response => {
      if (response.data.status === 1) {
        NotificationManager.success(translate('documentTypeManagement.deletedSuccess'));
        dispatch(fetchDocTypes(page));
        return dispatch(deleteDocumentTypeSuccess());
      } else {
        NotificationManager.error(translate('documentTypeManagement.deletedFailure'));
        return dispatch(deleteDocumentTypeFailure(response.data));
      }
    }, error => {
      NotificationManager.error(translate('documentTypeManagement.deletedFailure'));
      dispatch(deleteDocumentTypeFailure(error));
    })
  }
}

function changeDocType() {
  return {type: CHANGE_DOCUMENT_TYPE};
}

function changeDocTypeSuccess() {
  return {type: CHANGE_DOCUMENT_TYPE_SUCCESS};
}

function changeDocTypeFailure(error) {
  return {type: CHANGE_DOCUMENT_TYPE_FAILURE, error};
}

export function changeDocumentType(id, data) {
  return dispatch => {
    dispatch(changeDocType());
    axios.put(`/documentType/${id}`, data).then(response => {
      if (response.data.status === 1) {
        NotificationManager.success(translate('editDocType.successMessage'));
        dispatch(changeDocTypeSuccess());
      } else {
        NotificationManager.error(translate('editDocType.cantSave'));
      }
    }, error => {
      dispatch(changeDocTypeFailure(error));
    })
  }
}

function fetchDocumentType() {
  return {type: FETCH_DOCUMENT_TYPE};
}

function fetchDocumentTypeSuccess(docType) {
  return {type: FETCH_DOCUMENT_TYPE_SUCCESS, docType};
}

function fetchDocumentTypeFailure(error) {
  return {type: FETCH_DOCUMENT_TYPE_FAILURE, error};
}

export function fetchDocType(id) {
  return dispatch => {
    dispatch(fetchDocumentType());
    axios.get(`/documentType/${id}`).then(response => {
      if (response.data.status === 1) {
        dispatch(fetchDocumentTypeSuccess(response.data.object));
      } else {
        dispatch(fetchDocumentTypeFailure(response.data));
      }
    }, error => {
      dispatch(fetchDocumentTypeFailure(error));
    })
  }
}
