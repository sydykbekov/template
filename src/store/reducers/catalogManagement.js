import {
  FETCH_CATALOGS_NUMBER, FETCH_CATALOGS_NUMBER_SUCCESS, FETCH_CATALOGS_NUMBER_FAILURE,
  FETCH_CATALOGS, FETCH_CATALOGS_SUCCESS, FETCH_CATALOGS_FAILURE,
  ADD_CATALOG, ADD_CATALOG_SUCCESS, ADD_CATALOG_FAILURE,
  FETCH_CATALOG, FETCH_CATALOG_SUCCESS, FETCH_CATALOG_FAILURE
} from "../actionTypes";

const initialState = {
  catalogsNumber: 0,
  catalogs: [],
  isLoadingNumber: false,
  isLoading: false,
  error: null,
  catalog: {}
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CATALOGS_NUMBER:
      return {...state, isLoadingNumber: true, error: null, catalogsNumber: 0};
    case FETCH_CATALOGS_NUMBER_SUCCESS:
      return {...state, isLoadingNumber: false, catalogsNumber: action.catalogsNumber};
    case FETCH_CATALOGS_NUMBER_FAILURE:
      return {...state, isLoadingNumber: false, error: action.error};
    case FETCH_CATALOGS:
      return {...state, isLoading: true, error: null, catalogs: []};
    case FETCH_CATALOGS_SUCCESS:
      return {...state, isLoading: false, catalogs: action.catalogs};
    case FETCH_CATALOGS_FAILURE:
      return {...state, isLoading: false, error: action.error};
    case ADD_CATALOG:
      return {...state, isLoading: true, error: null};
    case ADD_CATALOG_SUCCESS:
      return {...state, isLoading: false};
    case ADD_CATALOG_FAILURE:
      return {...state, isLoading: false, error: action.error};
    case FETCH_CATALOG:
      return {...state, isLoading: true, error: null};
    case FETCH_CATALOG_SUCCESS:
      return {...state, isLoading: false, catalog: action.catalog};
    case FETCH_CATALOG_FAILURE:
      return {...state, isLoading: false, error: action.error};
    default:
      return state;
  }
}

export default reducer
