import {
  ADD_DOCUMENT_TYPE, ADD_DOCUMENT_TYPE_FAILURE, ADD_DOCUMENT_TYPE_SUCCESS, FETCH_DOCUMENT_TYPE,
  FETCH_DOCUMENT_TYPE_FAILURE,
  FETCH_DOCUMENT_TYPE_SUCCESS,
  FETCH_DOCUMENT_TYPES,
  FETCH_DOCUMENT_TYPES_SUCCESS,
  FETCH_DOCUMENTS_TYPE_FAILURE,
  FETCH_DOCUMENTS_TYPES_NUMBER, FETCH_DOCUMENTS_TYPES_NUMBER_SUCCESS, FETCH_DOCUMENTS_TYPES_NUMBER_FAILURE
} from "../actionTypes";

const initialState = {
  docTypesNumber: 0,
  docTypes: [],
  isLoadingNumber: false,
  isLoading: false,
  error: null,
  docType: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DOCUMENTS_TYPES_NUMBER:
      return {...state, isLoadingNumber: true, error: null, docTypesNumber: 0};
    case FETCH_DOCUMENTS_TYPES_NUMBER_SUCCESS:
      return {...state, isLoadingNumber: false, docTypesNumber: action.docTypesNumber};
    case FETCH_DOCUMENTS_TYPES_NUMBER_FAILURE:
      return {...state, isLoadingNumber: false, error: action.error};
    case FETCH_DOCUMENT_TYPES:
      return {...state, isLoading: true, error: null, docTypes: []};
    case FETCH_DOCUMENT_TYPES_SUCCESS:
      return {...state, isLoading: false, docTypes: action.types};
    case FETCH_DOCUMENTS_TYPE_FAILURE:
      return {...state, isLoading: false, error: action.error};
    case ADD_DOCUMENT_TYPE:
      return {...state, isLoading: true, error: null};
    case ADD_DOCUMENT_TYPE_SUCCESS:
      return {...state, isLoading: false};
    case ADD_DOCUMENT_TYPE_FAILURE:
      return {...state, isLoading: false, error: action.error};
    case FETCH_DOCUMENT_TYPE:
      return {...state, isLoading: true, error: null};
    case FETCH_DOCUMENT_TYPE_SUCCESS:
      return {...state, isLoading: false, docType: action.docType};
    case FETCH_DOCUMENT_TYPE_FAILURE:
      return {...state, isLoading: false, error: action.error};
    default:
      return state;
  }
};

export default reducer;