import {
  DELETE_DOCUMENT_SUCCESS, FETCH_DOCUMENT_FAILURE, FETCH_DOCUMENT_START, FETCH_DOCUMENT_SUCCESS,
  SEARCH_DOCUMENTS_FAILURE,
  SEARCH_DOCUMENTS_START,
  SEARCH_DOCUMENTS_SUCCESS
} from "../actionTypes";

const initialState = {
  foundDocs: [],
  searchingDocs: false,
  error: null,
  document: {},
  isLoading: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_DOCUMENTS_START:
      return {...state, searchingDocs: true, foundDocs: []};
    case SEARCH_DOCUMENTS_SUCCESS:
      return {...state, searchingDocs: false, foundDocs: action.foundDocs};
    case SEARCH_DOCUMENTS_FAILURE:
      return {...state, searchingDocs: false, error: action.error};
    case DELETE_DOCUMENT_SUCCESS:
      return {...state, foundDocs: state.foundDocs.filter(docs => docs.id !== action.id)};
    case FETCH_DOCUMENT_START:
      return {...state, isLoading: true};
    case FETCH_DOCUMENT_SUCCESS:
      return {...state, isLoading: false, document: action.document};
    case FETCH_DOCUMENT_FAILURE:
      return {...state, isLoading: false, error: action.error};
    default:
      return state;
  }
};

export default reducer;