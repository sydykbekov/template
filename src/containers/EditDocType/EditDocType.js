import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {translate} from "../../localization/i18n";
import {
  Button,
  TextField,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  OutlinedInput,
  FormControlLabel,
  Checkbox,
  LinearProgress
} from '@material-ui/core';
import axios from '../../axios-api';
import DeleteIcon from '@material-ui/icons/DeleteForeverOutlined';
import ReactDOM from "react-dom";
import {NotificationManager} from "react-notifications";
import {changeDocumentType} from "../../store/actions/docTypeManagement";
import cloneDeep from 'lodash/cloneDeep';

const metaTypes = [{label: 'Дата', value: 'date'}, {label: 'Текст', value: 'text'},
  {label: 'Число', value: 'number'}, {label: 'Булевое значение', value: 'bool'}];

class EditDocType extends Component {
  state = {
    loading: true
  };

  componentDidMount() {
    axios.get(`/documentType/${this.props.match.params.id}`).then(response => {
      if (response.data.status === 1) {
        const state = response.data.object;
        const meta = state.fieldsMeta[0];
        state.fieldsMeta.splice(0, 1);
        state.meta = meta;
        state.loading = false;
        delete state.id;
        this.setState(state);
      } else {
        this.setState({loading: false});
        return NotificationManager.error(translate('editDocType.errorMessage'));
      }
    }, () => {
      NotificationManager.error(translate('editDocType.errorMessage'));
      this.setState({loading: false});
    })
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.lang !== this.props.lang) {
      this.forceUpdate();
    }
  }

  submitFormHandler = event => {
    event.preventDefault();
    let data = cloneDeep(this.state);
    const isFieldTypes = data.fieldsMeta.every(meta => meta.fieldType.dataType);
    if (!isFieldTypes || !data.meta.fieldType.dataType) {
      return NotificationManager.error(translate('newDocType.fillTypes'));
    } else {
      data.fieldsMeta.unshift(data.meta);
      delete data.meta;
      delete data.loading;
      this.props.changeDocType(this.props.match.params.id, data);
    }
  };

  inputChangeHandler = event => {
    this.setState({[event.target.name]: event.target.value});
  };

  addMeta = () => {
    const fieldsMeta = [...this.state.fieldsMeta];
    const section = {
      kkFieldName: '',
      ruFieldName: '',
      enFieldName: '',
      fieldCode: '',
      fieldType: {
        dataType: '',
        recurrent: false
      },
      required: false
    };
    fieldsMeta.push(section);
    this.setState({fieldsMeta});
  };

  sectionChangeHandler = (event, id) => {
    const fieldsMeta = [...this.state.fieldsMeta];
    const section = fieldsMeta[id];
    section[event.target.name] = event.target.value;
    this.setState({fieldsMeta});
  };

  sectionCheckBoxHandler = (event, id) => {
    const fieldsMeta = [...this.state.fieldsMeta];
    const section = fieldsMeta[id];
    section[event.target.name] = !section[event.target.name];
    this.setState({fieldsMeta});
  };

  removeMeta = id => {
    const fieldsMeta = [...this.state.fieldsMeta];
    fieldsMeta.splice(id, 1);
    this.setState({fieldsMeta});
  };

  metaChangeHandler = event => {
    const meta = {...this.state.meta};
    meta[event.target.name] = event.target.value;
    this.setState({meta});
  };

  metaCheckBoxHandler = event => {
    const meta = {...this.state.meta};
    meta[event.target.name] = !meta[event.target.name];
    this.setState({meta});
  };

  sectionFieldTypeChangeHandler = (event, id) => {
    const fieldsMeta = cloneDeep(this.state.fieldsMeta);
    const section = fieldsMeta[id];
    if (event.target.name === 'recurrent') {
      section.fieldType.recurrent = !section.fieldType.recurrent;
      this.setState({fieldsMeta});
    } else {
      section.fieldType.dataType = event.target.value;
      this.setState({fieldsMeta});
    }
  };

  fieldTypeChangeHandler = event => {
    const meta = cloneDeep(this.state.meta);
    if (event.target.name === 'recurrent') {
      meta.fieldType.recurrent = !meta.fieldType.recurrent;
      this.setState({meta});
    } else {
      meta.fieldType.dataType = event.target.value;
      this.setState({meta});
    }
  };

  render() {
    const {loading, kkName, ruName, enName, kkDescription, ruDescription, enDescription, meta, fieldsMeta} = this.state;
    return (
      <Fragment>
        <h3>{translate('editDocType.title')}</h3>
        {loading ? <LinearProgress className="preLoader"/> :
          <form onSubmit={this.submitFormHandler} autoComplete="off">
            <div className="new-doc-type_form-container">
              <TextField
                label={translate('newDocType.kkName')}
                margin="dense"
                variant="outlined"
                name="kkName"
                value={kkName}
                onChange={this.inputChangeHandler}
                fullWidth
                required
              />
              <TextField
                label={translate('newDocType.ruName')}
                margin="dense"
                variant="outlined"
                name="ruName"
                value={ruName}
                onChange={this.inputChangeHandler}
                fullWidth
                required
              />
              <TextField
                label={translate('newDocType.enName')}
                margin="dense"
                variant="outlined"
                name="enName"
                value={enName}
                onChange={this.inputChangeHandler}
                fullWidth
              />
              <TextField
                label={translate('newDocType.kkDescription')}
                multiline
                rows="3"
                margin="dense"
                variant="outlined"
                name="kkDescription"
                value={kkDescription}
                onChange={this.inputChangeHandler}
                fullWidth
                required
              />
              <TextField
                label={translate('newDocType.ruDescription')}
                multiline
                rows="3"
                margin="dense"
                variant="outlined"
                name="ruDescription"
                value={ruDescription}
                onChange={this.inputChangeHandler}
                fullWidth
                required
              />
              <TextField
                label={translate('newDocType.enDescription')}
                multiline
                rows="3"
                margin="dense"
                variant="outlined"
                name="enDescription"
                value={enDescription}
                onChange={this.inputChangeHandler}
                fullWidth
              />
              <Button variant="contained" color="primary" type="submit"
                      className="new-doc-type_submit-btn">{translate('editDocType.saveChanges')}</Button>
            </div>
            <h4 className="meta-title">{translate('newDocType.metaTitle')}:</h4>
            <hr/>
            <div className="meta-add_container">
              <h5>{translate('newDocType.addMeta')}</h5>
              <Button variant="fab" mini color="primary" aria-label="Add" style={{fontSize: '20px'}}
                      onClick={this.addMeta}>+</Button>
            </div>
            <div className="meta-section">
              <TextField
                label={translate('newDocType.kkName')}
                margin="dense"
                variant="outlined"
                name="kkFieldName"
                value={meta.kkFieldName}
                onChange={this.metaChangeHandler}
                InputLabelProps={{
                  shrink: true
                }}
                required
              />
              <TextField
                label={translate('newDocType.ruName')}
                margin="dense"
                variant="outlined"
                name="ruFieldName"
                value={meta.ruFieldName}
                onChange={this.metaChangeHandler}
                InputLabelProps={{
                  shrink: true
                }}
                required
              />
              <TextField
                label={translate('newDocType.enName')}
                margin="dense"
                variant="outlined"
                name="enFieldName"
                value={meta.enFieldName}
                onChange={this.metaChangeHandler}
                InputLabelProps={{
                  shrink: true
                }}
              />
              <TextField
                label={translate('newDocType.code')}
                margin="dense"
                variant="outlined"
                name="fieldCode"
                value={meta.fieldCode}
                onChange={this.metaChangeHandler}
                InputLabelProps={{
                  shrink: true
                }}
                required
              />
              <FormControl variant="outlined" margin="dense" required>
                <InputLabel
                  ref={ref => {
                    this.labelRef = ReactDOM.findDOMNode(ref);
                  }}
                  shrink={true}
                  htmlFor="select-1"
                  style={{background: '#fff'}}
                >
                  {translate('newDocType.type')}
                </InputLabel>
                <Select
                  value={meta.fieldType.dataType}
                  onChange={this.fieldTypeChangeHandler}
                  name="dataType"
                  inputProps={{
                    id: 'select-1'
                  }}
                  input={
                    <OutlinedInput
                      labelWidth={this.labelRef ? this.labelRef.offsetWidth : 0}
                      name="select-1"
                      id="select-1"
                    />
                  }
                >
                  {metaTypes.map((type, key) =>
                    <MenuItem key={key} value={type.value}>{type.label}</MenuItem>
                  )}
                </Select>
              </FormControl>
              <div className="meta-section_checkbox-container">
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={typeof meta.required === 'string' ? JSON.parse(meta.required) : meta.required}
                      onChange={this.metaCheckBoxHandler}
                      color="primary"
                      name="required"
                    />
                  }
                  label={translate('newDocType.required')}
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={typeof meta.fieldType.recurrent === 'string' ? JSON.parse(meta.fieldType.recurrent) : meta.fieldType.recurrent}
                      onChange={this.fieldTypeChangeHandler}
                      color="primary"
                      name="recurrent"
                    />
                  }
                  label={translate('newDocType.recurrent')}
                />
              </div>
            </div>
            {fieldsMeta.map((section, id) =>
              <div key={id} className="meta-section">
                <TextField
                  label={translate('newDocType.kkName')}
                  margin="dense"
                  variant="outlined"
                  name="kkFieldName"
                  value={section.kkFieldName}
                  onChange={e => this.sectionChangeHandler(e, id)}
                  InputLabelProps={{
                    shrink: true
                  }}
                  required
                />
                <TextField
                  label={translate('newDocType.ruName')}
                  margin="dense"
                  variant="outlined"
                  name="ruFieldName"
                  value={section.ruFieldName}
                  onChange={e => this.sectionChangeHandler(e, id)}
                  InputLabelProps={{
                    shrink: true
                  }}
                  required
                />
                <TextField
                  label={translate('newDocType.enName')}
                  margin="dense"
                  variant="outlined"
                  name="enFieldName"
                  value={section.enFieldName}
                  onChange={e => this.sectionChangeHandler(e, id)}
                  InputLabelProps={{
                    shrink: true
                  }}
                />
                <TextField
                  label={translate('newDocType.code')}
                  margin="dense"
                  variant="outlined"
                  name="fieldCode"
                  value={section.fieldCode}
                  onChange={e => this.sectionChangeHandler(e, id)}
                  InputLabelProps={{
                    shrink: true
                  }}
                  required
                />
                <FormControl variant="outlined" margin="dense" required>
                  <InputLabel
                    ref={ref => {
                      this.labelRef = ReactDOM.findDOMNode(ref);
                    }}
                    shrink={true}
                    htmlFor="age-required"
                    style={{background: '#fff'}}
                  >
                    {translate('newDocType.type')}
                  </InputLabel>
                  <Select
                    value={section.fieldType.dataType}
                    onChange={e => this.sectionFieldTypeChangeHandler(e, id)}
                    name="dataType"
                    inputProps={{
                      id: 'age-required'
                    }}
                    input={
                      <OutlinedInput
                        labelWidth={this.labelRef ? this.labelRef.offsetWidth : 0}
                        name="age"
                        id="age-required"
                      />
                    }
                  >
                    {metaTypes.map((type, key) =>
                      <MenuItem key={key} value={type.value}>{type.label}</MenuItem>
                    )}
                  </Select>
                </FormControl>
                <div className="meta-section_checkbox-container">
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={typeof section.required === 'string' ? JSON.parse(section.required) : section.required}
                        onChange={e => this.sectionCheckBoxHandler(e, id)}
                        color="primary"
                        name="required"
                      />
                    }
                    label={translate('newDocType.required')}
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={typeof section.fieldType.recurrent === 'string' ? JSON.parse(section.fieldType.recurrent) : section.fieldType.recurrent}
                        onChange={e => this.sectionFieldTypeChangeHandler(e, id)}
                        color="primary"
                        name="recurrent"
                      />
                    }
                    label={translate('newDocType.recurrent')}
                  />
                </div>
                <Button color="secondary" onClick={() => this.removeMeta(id)}><DeleteIcon/></Button>
              </div>)}
          </form>}
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  lang: state.localization.language.language
});

const mapDispatchToProps = dispatch => ({
  changeDocType: (id, data) => dispatch(changeDocumentType(id, data))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditDocType);