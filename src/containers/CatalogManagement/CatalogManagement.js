import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button} from "@material-ui/core";
import SearchBox from "../../components/SearchBox/SearchBox";
import DataTable from "../../components/DataTable/DataTable";
import ConfirmationWindow from "../../components/ConfirmationWindow/ConfirmationWindow";
import {translate} from "../../localization/i18n";
import {fetchCatalogsNumber, fetchCatalogs, deleteCatalog} from "../../store/actions/catalogManagement";
import {searchCatalog} from "../../store/actions/search";
import './CatalogManagement.css';

class CatalogManagement extends Component {
  state = {
    isOpen: false,
    id: null,
    found: null,
    currentPage: 1
  };

  componentDidMount() {
    this.props.fetchCatalogsNumber();
    this.props.fetchCatalogs(1);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.lang !== this.props.lang) {
      this.forceUpdate();
    }

    if (prevState.currentPage !== this.state.currentPage) {
      this.props.fetchCatalogsNumber();
      this.props.fetchCatalogs(this.state.currentPage);
    }
  }

  editCatalog = id => {
    this.props.history.push(`catalogs/edit/${id}`);
  };

  deleteCatalog = () => {
    this.props.deleteCatalog(this.state.id, this.state.currentPage);
    this.setState({isOpen: false});
  };

  confirm = id => {
    this.setState({isOpen: true, id});
  };

  newCatalog = () => {
    this.props.history.push('/catalogs/new_catalog');
  };

  viewCatalog = id => {
    this.props.history.push(`view_catalog/${id}`);
  };

  handleClose = () => {
    this.setState({isOpen: false});
  };

  selectChangeHandler = (e) => {
    const word = e.target.value;

    if (word.length >= 3) {
      this.props.searchByWord(word);
    }
  };

  selectTranslation(kz, ru) {
    switch (this.props.lang) {
      case 'KZ_kz':
        return kz;
      default:
        return ru;
    }
  }

  filledHandler = value => {
    const found = [{...value}];
    this.setState({found});
  };

  firstPage = () => {
    this.setState({currentPage: 1});
  };

  lastPage = () => {
    this.setState({currentPage: this.props.catalogsNumber});
  };

  nextPage = () => {
    this.setState({currentPage: this.state.currentPage + 1});
  };

  prevPage = () => {
    this.setState({currentPage: this.state.currentPage - 1});
  };

  render() {
    const {isLoadingNumber, isLoading, foundCatalogs, searching} = this.props;
    const suggestions = foundCatalogs.map(type => ({
      label: type.ruName,
      id: type.id,
      kkName: type.kkName,
      ruName: type.ruName
    }));

    return (
      <Fragment>
        <h3>{translate('catalogManagement.title')}</h3>
        <Button
          onClick={this.newCatalog}
          variant="contained"
          color="primary">{translate('catalogManagement.button')}
        </Button>
        <SearchBox
          placeholder={translate('catalogManagement.searchBoxPlaceholder')}
          array={suggestions}
          selectChangeHandler={this.selectChangeHandler}
          filledHandler={this.filledHandler}
          isLoading={searching}
          className="top-right_search-box"
          icon="block"
        />
        <DataTable
          numPages={this.props.catalogsNumber}
          curPage={this.state.currentPage}
          onFirstPage={this.firstPage}
          onLastPage={this.lastPage}
          onNextPage={this.nextPage}
          onPrevPage={this.prevPage}
          isLoading={isLoadingNumber || isLoading}
          rows={(this.state.found || this.props.catalogs)
            .map(type => {
              return {
                id: type.id,
                cols: [
                  {value: () => this.selectTranslation(type.kkName, type.ruName), cb: () => this.viewCatalog(type.id)},
                  {value: () => translate('catalogManagement.edit'), cb: () => this.editCatalog(type.id)},
                  {value: () => translate('catalogManagement.delete'), cb: () => this.confirm(type.id)},
                ]
              };
            })
          }
        />
        <ConfirmationWindow
          isOpen={this.state.isOpen}
          handleClose={this.handleClose}
          delete={this.deleteCatalog}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  lang: state.localization.language.language,
  catalogsNumber: state.catalogManagement.catalogsNumber,
  catalogs: state.catalogManagement.catalogs,
  isLoadingNumber: state.catalogManagement.isLoadingNumber,
  isLoading: state.catalogManagement.isLoading,
  foundCatalogs: state.search.result,
  searching: state.search.isLoading
});

const mapDispatchToProps = dispatch => ({
  fetchCatalogsNumber: () => dispatch(fetchCatalogsNumber()),
  fetchCatalogs: page => dispatch(fetchCatalogs(page)),
  deleteCatalog: (id, page) => dispatch(deleteCatalog(id, page)),
  searchByWord: word => dispatch(searchCatalog(word))
});

export default connect(mapStateToProps, mapDispatchToProps)(CatalogManagement);
