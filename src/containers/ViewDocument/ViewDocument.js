import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {translate} from "../../localization/i18n";
import {
  TextField,
  Button,
  LinearProgress,
  FormControlLabel,
  Checkbox
} from '@material-ui/core';
import {fetchCatalog, fetchCatalogs} from "../../store/actions/catalogManagement";
import {fetchDocType, fetchDocTypes} from "../../store/actions/docTypeManagement";
import {addDoc, fetchDocument} from "../../store/actions/workWithDocs";
import './ViewDocument.css';

class EditDocument extends Component {
  state = {
    catalog: '',
    documentType: '',
    signature: '',
    file: '',
    binaryData: '',
    fieldsMeta: [],
    foundCatalogs: [],
    foundDocTypes: []
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.lang !== this.props.lang ||
      (prevState.documentType && prevState.documentType !== this.state.documentType)) {
      this.forceUpdate();
    }

    const {catalog, documentType} = this.props;

    if (!this.state.catalog && catalog.id) {
      this.setState({catalog: {kkName: catalog.kkName, ruName: catalog.ruName, value: catalog.id}});
    }

    if (!this.state.documentType && documentType.id) {
      const fieldsMeta = documentType.fieldsMeta.map(meta => {
        if (isRecurrent(meta.fieldType.recurrent)) {
          return {...meta, fieldValue: [{value: ''}]};
        } else {
          return {...meta, fieldValue: ''};
        }
      });
      this.setState({
        documentType: {
          kkName: documentType.kkName,
          ruName: documentType.ruName,
          value: documentType.id
        },
        fieldsMeta
      });
    }
  }

  componentDidMount() {
    const {dirID, typeID, id} = this.props.match.params;
    this.props.getDocument(dirID, id);
    this.props.getDocumentType(typeID);
    this.props.getCatalog(dirID);
  }

  render() {
    const {lang, document, documentType, catalog} = this.props;
    const {isDocLoading, isDocTypeLoading, isCatalogLoading} = this.props;
    let metaLang, typeLang, isLoading, metas;
    if (lang === 'RU_ru') {
      metaLang = 'ruFieldName';
      typeLang = 'ruName';
    } else {
      metaLang = 'kkFieldName';
      typeLang = 'kkName';
    }
    isLoading = isDocLoading || isDocTypeLoading || isCatalogLoading;
    metas = [];
    if (document.data && documentType.fieldsMeta) {
      document.data.forEach(doc => {
        documentType.fieldsMeta.forEach(type => {
          if (type.fieldCode === doc.fieldCode) {
            const obj = {...type};
            obj.fieldValue = doc.fieldValue;
            metas.push(obj);
          }
        })
      })
    }
    return (
      <Fragment>
        <h3>{translate('viewDoc.title')}</h3>
        {isLoading ? <LinearProgress className="preLoader"/> :
          <form className="new-doc-type_form-container" onSubmit={this.submitFormHandler} autoComplete="off">
            <TextField
              label={translate('viewDoc.catalog')}
              margin="dense"
              variant="outlined"
              value={catalog[typeLang]}
              fullWidth
              InputLabelProps={{shrink: true}}
              InputProps={{readOnly: true}}
            />
            <TextField
              label={translate('viewDoc.docType')}
              margin="dense"
              variant="outlined"
              value={documentType[typeLang]}
              fullWidth
              InputLabelProps={{shrink: true}}
              InputProps={{readOnly: true}}
            />
            <TextField
              label={translate('newDoc.edc')}
              margin="dense"
              variant="outlined"
              value={document.signature}
              fullWidth
              rows="4"
              InputLabelProps={{shrink: true}}
              InputProps={{readOnly: true}}
              multiline
            />
            <TextField
              label={translate('newDoc.upload')}
              margin="dense"
              variant="outlined"
              value={document.fileName}
              InputLabelProps={{shrink: true}}
              InputProps={{readOnly: true}}
              fullWidth
            />
            <h4 className="new-doc-type_meta-title">{translate('newDoc.metaTitle')}:</h4>
            <hr/>
            {metas.map((meta, key) =>
              meta.fieldType.dataType === 'bool' ?
                <div className="new-doc_meta-section-container" key={key}>
                  {isRecurrent(meta.fieldType.recurrent) ?
                    <Fragment>
                      {meta.fieldValue.map((field, id) =>
                        <Fragment key={id}>
                          <FormControlLabel
                            control={
                              <Checkbox
                                color="primary"
                                name="value"
                                required={typeof meta.required === 'string' ? JSON.parse(meta.required) : meta.required}
                              />
                            }
                            label={`${meta[metaLang]}${meta.enFieldName ? `/${meta.enFieldName}` : ''}`}
                          />
                        </Fragment>
                      )}
                    </Fragment>
                    :
                    <FormControlLabel
                      control={
                        <Checkbox
                          color="primary"
                          name="required"
                          required={typeof meta.required === 'string' ? JSON.parse(meta.required) : meta.required}
                        />
                      }
                      label={`${meta[metaLang]}${meta.enFieldName ? `/${meta.enFieldName}` : ''}`}
                    />
                  }
                </div>
                :
                <div className="new-doc_meta-section-container" key={key}>
                  {isRecurrent(meta.fieldType.recurrent) ?
                    <Fragment>
                      {meta.fieldValue.map((field, id) =>
                        <TextField
                          key={id}
                          margin="dense"
                          variant="outlined"
                          type={meta.fieldType.dataType}
                          label={`${meta[metaLang]}${meta.enFieldName ? `/${meta.enFieldName}` : ''}`}
                          value={field.value}
                          fullWidth
                          InputLabelProps={{shrink: true}}
                          InputProps={{readOnly: true}}
                        />
                      )}
                    </Fragment>
                    :
                    <TextField
                      margin="dense"
                      variant="outlined"
                      type={meta.fieldType.dataType}
                      label={`${meta[metaLang]}${meta.enFieldName ? `/${meta.enFieldName}` : ''}`}
                      value={meta.fieldValue}
                      fullWidth
                      InputLabelProps={{shrink: true}}
                      InputProps={{readOnly: true}}
                    />
                  }
                </div>
            )}
            <Button variant="contained" color="primary" className="new-doc-type_submit-btn"
                    type="submit">{translate('editDoc.saveChanges')}</Button>
          </form>
        }
      </Fragment>
    );
  }
}

function isRecurrent(recurrent) {
  if (typeof recurrent === 'string') {
    return JSON.parse(recurrent);
  } else {
    return recurrent;
  }
}

const mapStateToProps = state => ({
  lang: state.localization.language.language,
  docTypes: state.docTypeManagement.docTypes,
  catalogs: state.catalogManagement.catalogs,
  isFetchingDocTypes: state.docTypeManagement.isLoading,
  isFetchingCatalogs: state.catalogManagement.isLoading,
  document: state.workWithDocs.document,
  documentType: state.docTypeManagement.docType,
  catalog: state.catalogManagement.catalog,
  idDocLoading: state.workWithDocs.isLoading,
  isDocTypeLoading: state.docTypeManagement.isLoading,
  isCatalogLoading: state.catalogManagement.isLoading
});

const mapDispatchToProps = dispatch => ({
  fetchDocTypes: page => dispatch(fetchDocTypes(page)),
  fetchCatalogs: page => dispatch(fetchCatalogs(page)),
  addDoc: (formData, dirID) => dispatch(addDoc(formData, dirID)),
  getDocument: (dirID, docID) => dispatch(fetchDocument(dirID, docID)),
  getDocumentType: id => dispatch(fetchDocType(id)),
  getCatalog: id => dispatch(fetchCatalog(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditDocument);
