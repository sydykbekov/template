import React, {Component, Fragment} from 'react';
import {NotificationManager} from 'react-notifications';
import {connect} from "react-redux";
import Select from 'react-select';
import PropTypes from 'prop-types';
import cloneDeep from 'lodash/cloneDeep';
import {translate} from "../../localization/i18n";
import {Button, TextField, Checkbox, FormControlLabel} from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import Chip from '@material-ui/core/Chip';
import CancelIcon from '@material-ui/icons/Cancel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import {LinearProgress} from '@material-ui/core';
import classNames from 'classnames';
import {withStyles} from '@material-ui/core/styles';
import {emphasize} from '@material-ui/core/styles/colorManipulator';
import DynamicDataTable from "../../components/DynamicDataTable/DynamicDataTable";
import {searchCatalogs, searchDocTypes, searchDocuments, resetSearchDocuments} from "../../store/actions/search";
import {fetchCatalogs} from "../../store/actions/catalogManagement";
import {fetchDocTypes} from "../../store/actions/docTypeManagement";
import {deleteDocument} from "../../store/actions/workWithDocs";
import ConfirmationWindow from "../../components/ConfirmationWindow/ConfirmationWindow";
import {maybeParseJSONString} from "../../components/Utils";
import './Search.css';

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: 250,
  },
  input: {
    display: 'flex',
    padding: 0,
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
      0.08,
    ),
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
  },
  singleValue: {
    fontSize: 16,
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 16,
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
  divider: {
    height: theme.spacing.unit * 2,
  },
});

function NoOptionsMessage(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.noOptionsMessage}
      {...props.innerProps}
    >
      {translate('search.noMessage')}
    </Typography>
  );
}

function inputComponent({inputRef, ...props}) {
  return <div ref={inputRef} {...props} />;
}

function Control(props) {
  return (
    <TextField
      fullWidth
      onChange={event => props.selectProps.onType(event.target.value)}
      InputProps={{
        inputComponent,
        inputProps: {
          className: props.selectProps.classes.input,
          inputRef: props.innerRef,
          children: props.children,
          ...props.innerProps,
        },
      }}
      {...props.selectProps.textFieldProps}
    />
  );
}

function Option(props) {
  return (
    <MenuItem
      buttonRef={props.innerRef}
      selected={props.isFocused}
      component="div"
      style={{
        fontWeight: props.isSelected ? 500 : 400,
      }}
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

function Placeholder(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.placeholder}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function SingleValue(props) {
  return (
    <Typography className={props.selectProps.classes.singleValue} {...props.innerProps}>
      {props.children}
    </Typography>
  );
}

function ValueContainer(props) {
  return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>;
}

function MultiValue(props) {
  return (
    <Chip
      tabIndex={-1}
      label={props.children}
      className={classNames(props.selectProps.classes.chip, {
        [props.selectProps.classes.chipFocused]: props.isFocused,
      })}
      onDelete={props.removeProps.onClick}
      deleteIcon={<CancelIcon {...props.removeProps} />}
    />
  );
}

function Menu(props) {
  return (
    <Paper square className={props.selectProps.classes.paper} {...props.innerProps}>
      {props.children}
    </Paper>
  );
}

const components = {
  Control,
  Menu,
  MultiValue,
  NoOptionsMessage,
  Option,
  Placeholder,
  SingleValue,
  ValueContainer,
};

const MetaField = props => {
  const {kkey, index, metaLang, meta} = props;
  const {
    addMetaFieldRecurrent, removeMetaFieldRecurrent,
    changeMetaFieldValue, changeMetaFieldRecurrentValue,
    changeCheckBoxMetaField, changeCheckBoxMetaFieldRecurrent,
    changeCheckBoxExactMatch
  } = props;

  return (
    meta.fieldType.dataType === 'bool' ?
      maybeParseJSONString(meta.fieldType.recurrent) ?
        <Fragment>
          {meta.fieldValue.map((field, id) =>
            <Fragment key={id}>
              <FormControlLabel
                control={
                  <Checkbox
                    onChange={event => changeCheckBoxMetaFieldRecurrent(event, id, kkey, index)}
                    color="primary"
                    name="value"
                    required={typeof meta.required === 'string' ? JSON.parse(meta.required) : meta.required}
                  />
                }
                label={`${meta[metaLang]}${meta.enFieldName ? `/${meta.enFieldName}` : ''}`}
              />
            </Fragment>
          )}
          <FormControlLabel
            control={
              <Checkbox
                checked={meta.exactMatch}
                onChange={e => changeCheckBoxExactMatch(e, kkey, index)}
                color="primary"
                name="exactMatch"
              />
            }
            label={translate('search.exactCheckbox')}
          />
          <Button
            variant="fab" mini
            color="primary"
            aria-label="Add"
            onClick={() => addMetaFieldRecurrent(kkey, index)}>
            +
          </Button>
          {meta.fieldValue.length > 1 &&
          <Button
            variant="fab" mini
            color="secondary"
            aria-label="Delete"
            style={{right: '30px'}}
            onClick={() => removeMetaFieldRecurrent(kkey, index)}>
            -
          </Button>}
        </Fragment>
        :
        <Fragment>
          <FormControlLabel
            control={
              <Checkbox
                onChange={event => changeCheckBoxMetaField(event, kkey, index)}
                color="primary"
                name="required"
                required={typeof meta.required === 'string' ? JSON.parse(meta.required) : meta.required}
              />
            }
            label={`${meta[metaLang]}${meta.enFieldName ? `/${meta.enFieldName}` : ''}`}
          />
          <FormControlLabel
            control={
              <Checkbox
                checked={meta.exactMatch}
                onChange={e => changeCheckBoxExactMatch(e, kkey, index)}
                color="primary"
                name="exactMatch"
              />
            }
            label={translate('search.exactCheckbox')}
          />
        </Fragment>
      :
      maybeParseJSONString(meta.fieldType.recurrent) ?
        <Fragment>
          {meta.fieldValue.map((field, id) =>
            <TextField
              key={id}
              margin="dense"
              variant="outlined"
              name="fieldValue"
              type={meta.fieldType.dataType}
              label={`${meta[metaLang]}${meta.enFieldName ? `/${meta.enFieldName}` : ''}`}
              value={field.value}
              onChange={e => changeMetaFieldRecurrentValue(e, id, kkey, index)}
              required={typeof meta.required === 'string' ? JSON.parse(meta.required) : meta.required}
              fullWidth
              InputLabelProps={{
                shrink: true,
              }}
            />
          )}
          <FormControlLabel
            control={
              <Checkbox
                checked={meta.exactMatch}
                onChange={e => changeCheckBoxExactMatch(e, kkey, index)}
                color="primary"
                name="exactMatch"
              />
            }
            label={translate('search.exactCheckbox')}
          />
          <Button
            variant="fab" mini
            color="primary"
            aria-label="Add"
            onClick={() => addMetaFieldRecurrent(kkey, index)}>
            +
          </Button>
          {meta.fieldValue.length > 1 &&
          <Button
            variant="fab" mini
            color="secondary"
            aria-label="Delete"
            style={{right: '30px'}}
            onClick={() => removeMetaFieldRecurrent(kkey, index)}>
            -
          </Button>}
        </Fragment>
        :
        <Fragment>
          <TextField
            margin="dense"
            variant="outlined"
            name="fieldValue"
            type={meta.fieldType.dataType}
            label={`${meta[metaLang]}${meta.enFieldName ? `/${meta.enFieldName}` : ''}`}
            value={meta.fieldValue}
            onChange={e => changeMetaFieldValue(e, kkey, index)}
            required={typeof meta.required === 'string' ? JSON.parse(meta.required) : meta.required}
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
          />
          <FormControlLabel
            control={
              <Checkbox
                checked={meta.exactMatch}
                onChange={e => changeCheckBoxExactMatch(e, kkey, index)}
                color="primary"
                name="exactMatch"
              />
            }
            label={translate('search.exactCheckbox')}
          />
        </Fragment>
  );
};

MetaField.propTypes = {
  kkey: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  metaLang: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  addMetaFieldRecurrent: PropTypes.func.isRequired,
  removeMetaFieldRecurrent: PropTypes.func.isRequired,
  changeMetaFieldValue: PropTypes.func.isRequired,
  changeMetaFieldRecurrentValue: PropTypes.func.isRequired,
  changeCheckBoxMetaField: PropTypes.func.isRequired,
  changeCheckBoxMetaFieldRecurrent: PropTypes.func.isRequired,
  changeCheckBoxExactMatch: PropTypes.func.isRequired,
};

const MetaFields = ({
                      index, metaLang, metaFields,
                      addMetaFieldRecurrent, removeMetaFieldRecurrent,
                      changeMetaFieldValue, changeMetaFieldRecurrentValue,
                      changeCheckBoxMetaField, changeCheckBoxMetaFieldRecurrent,
                      changeCheckBoxExactMatch
                    }) =>
  metaFields.map((meta, key) =>
    <div
      key={key}
      className="search_meta-section-container">
      <MetaField
        kkey={key}
        index={index}
        metaLang={metaLang}
        meta={meta}
        addMetaFieldRecurrent={addMetaFieldRecurrent}
        removeMetaFieldRecurrent={removeMetaFieldRecurrent}
        changeMetaFieldValue={changeMetaFieldValue}
        changeMetaFieldRecurrentValue={changeMetaFieldRecurrentValue}
        changeCheckBoxMetaField={changeCheckBoxMetaField}
        changeCheckBoxMetaFieldRecurrent={changeCheckBoxMetaFieldRecurrent}
        changeCheckBoxExactMatch={changeCheckBoxExactMatch}
      />
    </div>);

MetaFields.propTypes = {
  index: PropTypes.number.isRequired,
  metaLang: PropTypes.string.isRequired,
  metaFields: PropTypes.array.isRequired,
  addMetaFieldRecurrent: PropTypes.func.isRequired,
  removeMetaFieldRecurrent: PropTypes.func.isRequired,
  changeMetaFieldValue: PropTypes.func.isRequired,
  changeMetaFieldRecurrentValue: PropTypes.func.isRequired,
  changeCheckBoxMetaField: PropTypes.func.isRequired,
  changeCheckBoxMetaFieldRecurrent: PropTypes.func.isRequired,
  changeCheckBoxExactMatch: PropTypes.func.isRequired,
};

class Search extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedCatalogs: [],
      selectedDocTypes: [],
      filename: '',
      dateFrom: '',
      dateTo: '',
      isOpen: false,
      typesMetaFields: [],
    };
  }

  componentDidMount() {
    this.props.fetchCatalogs(1);
    this.props.fetchDocTypes(1);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.lang !== this.props.lang) {
      this.forceUpdate();
    }

    const {selectedDocTypes} = this.state;

    if (selectedDocTypes.length !== prevState.selectedDocTypes.length) {
      const typesMetaFields = selectedDocTypes.map(docType => Search.addMetaFieldsValues(docType.typesMetaFields));
      this.setState({typesMetaFields});
    }
  }

  handleMultiSelectionChange = name => value => {
    this.props.resetSearchDocuments();

    this.setState({[name]: value});
  };

  inputChangeHandler = (key, value) => {
    this.setState({[key]: value});
  };

  onCatalogSearch = (e) => {
    if (e.length >= 3) {
      this.props.searchCatalogs(e);
    }
  };

  onDocTypeSearch = (e) => {
    if (e.length >= 3) {
      this.props.searchDocTypes(e);
    }
  };

  editEntry = () => {
    console.log("Edit entry!");
  };

  viewDocument = id => {
    console.log(id);
  };

  confirm = (dirID, docID) => {
    this.setState({isOpen: true, dirID, docID});
  };

  deleteDoc = () => {
    this.props.deleteDoc(this.state.dirID, this.state.docID);
    this.setState({isOpen: false});
  };

  handleClose = () => {
    this.setState({isOpen: false});
  };

  submitFormHandler = event => {
    event.preventDefault();

    const {selectedCatalogs, selectedDocTypes, typesMetaFields} = this.state;

    if (selectedCatalogs.length < 1 || selectedDocTypes < 1) {
      NotificationManager.error(translate('search.requiredData'));

      return;
    }

    const typesFields = typesMetaFields.map(fields =>
      fields
        .filter(field => !Array.isArray(field.fieldValue))
        .map(field => {
          return {
            fieldCode: field.fieldCode,
            fieldValue: field.fieldValue,
            operation: field.exactMatch ? "exact" : "contains",
          };
        }));
    // FIXME: recurrent doesn't work in API requests
    /*
    if (Array.isArray(field.fieldValue)) {
      return {
        fieldCode: field.fieldCode,
        fieldValue: field.fieldValue.map(value => value.value),
        operation: field.exactMatch ? "exact" : "contains",
      };
    } else {
      return {
        fieldCode: field.fieldCode,
        fieldValue: field.fieldValue,
        operation: field.exactMatch ? "exact" : "contains",
      };
    }
    */
    //}));

    const formData = {
      directories: selectedCatalogs.map(({value}) => value),
      documentTypes: selectedDocTypes.map(({value}) => value),
      dateFrom: this.state.dateFrom,
      dateTo: this.state.dateTo,
      fileName: this.state.filename,
      fields: [].concat.apply([], typesFields),
    };

    this.props.searchDocuments(formData);
  };

  addMetaFieldRecurrent = (id, index) => {
    const typesMetaFields = cloneDeep(this.state.typesMetaFields);
    typesMetaFields[index][id].fieldValue.push({value: ''});
    this.setState({typesMetaFields});
  };

  removeMetaFieldRecurrent = (id, index) => {
    const typesMetaFields = cloneDeep(this.state.typesMetaFields);
    typesMetaFields[index][id].fieldValue.splice(-1, 1);
    this.setState({typesMetaFields});
  };

  changeMetaFieldValue = (event, id, index) => {
    const typesMetaFields = cloneDeep(this.state.typesMetaFields);
    typesMetaFields[index][id].fieldValue = event.target.value;
    this.setState({typesMetaFields});
  };

  changeMetaFieldRecurrentValue = (event, id, key, index) => {
    const typesMetaFields = cloneDeep(this.state.typesMetaFields);
    typesMetaFields[index][key].fieldValue[id].value = event.target.value;
    this.setState({typesMetaFields});
  };

  changeCheckBoxMetaField = (event, id, index) => {
    const typesMetaFields = cloneDeep(this.state.typesMetaFields);
    typesMetaFields[index][id].fieldValue = !event.target.value;
    this.setState({typesMetaFields});
  };

  changeCheckBoxMetaFieldRecurrent = (event, id, key, index) => {
    const typesMetaFields = cloneDeep(this.state.typesMetaFields);
    typesMetaFields[index][key].fieldValue[id].value = !typesMetaFields[key].fieldValue[id].value;
    this.setState({typesMetaFields});
  };

  changeCheckBoxExactMatch = (event, id, index) => {
    const typesMetaFields = cloneDeep(this.state.typesMetaFields);
    typesMetaFields[index][id].exactMatch = !event.target.value;
    this.setState({typesMetaFields});
  };

  selectTranslation(kz, ru) {
    switch (this.props.lang) {
      case 'KZ_kz':
        return kz;
      default:
        return ru;
    }
  }

  makeSuggestions(options) {
    return options.map(obj => {
      const selectLang = () => this.selectTranslation(obj.kkName, obj.ruName);

      return {
        value: obj.id,
        label: selectLang(),
        dynLabel: selectLang,
        typesMetaFields: obj.fieldsMeta || [],
      };
    });
  }

  static orList(list1, list2) {
    if (list1.length > 0) return list1;
    if (list2.length > 0) return list2;
    return [];
  }

  static addMetaFieldsValues(metaFields) {
    return metaFields.map(meta => {
      if (maybeParseJSONString(meta.fieldType.recurrent)) {
        return {...meta, fieldValue: [{value: ''}], exactMatch: false};
      } else {
        return {...meta, fieldValue: '', exactMatch: false};
      }
    });
  }

  render() {
    const {classes, theme, lang} = this.props;

    const selectStyles = {
      input: base => ({
        ...base,
        color: theme.palette.text.primary,
        '& input': {
          font: 'inherit',
        },
      }),
    };

    let metaLang;
    if (lang === 'RU_ru') {
      metaLang = 'ruFieldName';
    } else {
      metaLang = 'kkFieldName';
    }

    const {catalogs, isCatalogsLoading, docTypes, isDocTypesLoading} = this.props;
    const {foundCatalogs, foundDocTypes} = this.props;

    const catalogSuggestions = this.makeSuggestions(Search.orList(foundCatalogs, catalogs));
    const docTypeSuggestions = this.makeSuggestions(Search.orList(foundDocTypes, docTypes));

    const {foundDocs, isSearchingDocs} = this.props;
    const {selectedCatalogs, selectedDocTypes} = this.state;

    const catalogNameById = id =>
      selectedCatalogs.filter(({value}) => value === id)[0].dynLabel;

    const docTypeNameById = id =>
      selectedDocTypes.filter(({value}) => value === id)[0].dynLabel;

    const docs = foundDocs.map(doc => ({
      id: doc.id,
      cols: [
        {value: doc.fileName, cb: () => this.viewDocument(doc.id)},
        {value: catalogNameById(doc.directoryId)},
        {value: docTypeNameById(doc.documentType)},
        {
          value: () => translate('search.edit'),
          cb: () => this.editEntry
        },
        {
          value: () => translate('search.delete'),
          cb: () => this.confirm(doc.directoryId, doc.id)
        },
      ]
    }));

    return (
      <Fragment>
        <h3>{translate('search.title')}</h3>
        {(isCatalogsLoading || isDocTypesLoading)
          ?
          <LinearProgress className="preLoader"/>
          :
          <form
            className="search_form-container"
            autoComplete="off"
            onSubmit={this.submitFormHandler}>
            <Select
              classes={classes}
              textFieldProps={{
                label: translate('search.catalog'),
                InputLabelProps: {
                  shrink: true,
                },
              }}
              styles={selectStyles}
              options={catalogSuggestions}
              variant="outline"
              components={components}
              value={selectedCatalogs}
              onChange={this.handleMultiSelectionChange('selectedCatalogs')}
              onType={this.onCatalogSearch}
              placeholder={translate('search.catalog')}
              isMulti
            />
            <Select
              classes={classes}
              textFieldProps={{
                label: translate('search.docType'),
                InputLabelProps: {
                  shrink: true,
                },
              }}
              styles={selectStyles}
              options={docTypeSuggestions}
              variant="outline"
              components={components}
              value={selectedDocTypes}
              onChange={this.handleMultiSelectionChange('selectedDocTypes')}
              onType={this.onDocTypeSearch}
              placeholder={translate('search.docType')}
              isMulti
            />
            <TextField
              label={translate('search.filename')}
              margin="dense"
              variant="outlined"
              name="filename"
              className="search_form-container_filename-input"
              value={this.state.filename}
              onChange={event => this.inputChangeHandler('filename', event.target.value)}
              fullWidth
            />
            <div className="search">
              <TextField
                variant="outlined"
                margin="dense"
                label={translate('search.dateFrom')}
                type="date"
                value={this.state.dateFrom}
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={event => this.inputChangeHandler('dateFrom', event.target.value)}
              />
              <TextField
                variant="outlined"
                margin="dense"
                label={translate('search.dateTo')}
                type="date"
                value={this.state.dateTo}
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={event => this.inputChangeHandler('dateTo', event.target.value)}
              />
            </div>
            <Button
              variant="contained"
              color="primary"
              type="submit"
              style={{display: 'block', clear: 'both'}}
              className="search_submit-btn">{translate('search.find')}
            </Button>
          </form>
        }
        {
          this.state.typesMetaFields.length > 0 &&
          <div>
            <h4 className="search_meta-title">{translate('search.metaTitle')}:</h4>
            <hr/>
            {this.state.typesMetaFields.map((meta, index) =>
              <MetaFields
                key={index}
                index={index}
                metaLang={metaLang}
                metaFields={meta}
                addMetaFieldRecurrent={this.addMetaFieldRecurrent}
                removeMetaFieldRecurrent={this.removeMetaFieldRecurrent}
                changeMetaFieldValue={this.changeMetaFieldValue}
                changeMetaFieldRecurrentValue={this.changeMetaFieldRecurrentValue}
                changeCheckBoxMetaField={this.changeCheckBoxMetaField}
                changeCheckBoxMetaFieldRecurrent={this.changeCheckBoxMetaFieldRecurrent}
                changeCheckBoxExactMatch={this.changeCheckBoxExactMatch}
              />)}
          </div>
        }
        <h4 className="search_meta-title">{translate('search.resultsTitle')}:</h4>
        <hr/>
        <DynamicDataTable
          header={[
            {id: "filename", label: translate('search.colFilename')},
            {id: "catalog", label: translate('search.colCatalog')},
            {id: "doc_type", label: translate('search.colDocType')},
            {id: null},
            {id: null},
          ]}
          rows={docs}
          isLoading={isSearchingDocs}
          orderBy='filename'
        />
        <ConfirmationWindow
          isOpen={this.state.isOpen}
          handleClose={this.handleClose}
          delete={this.deleteDoc}
        />
      </Fragment>
    );
  }
}

Search.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  lang: state.localization.language.language,
  catalogs: state.catalogManagement.catalogs,
  isCatalogsLoading: state.catalogManagement.isLoading,
  docTypes: state.docTypeManagement.docTypes,
  isDocTypesLoading: state.docTypeManagement.isLoading,
  foundCatalogs: state.search.foundCatalogs,
  isSearchingCatalogs: state.search.isSearchingCatalogs,
  foundDocTypes: state.search.foundDocTypes,
  isSearchingDocTypes: state.search.isSearchingDocTypes,
  foundDocs: state.search.foundDocs,
  isSearchingDocs: state.search.isSearchingDocs,
});

const mapDispatchToProps = dispatch => ({
  fetchCatalogs: page => dispatch(fetchCatalogs(page)),
  fetchDocTypes: page => dispatch(fetchDocTypes(page)),
  searchCatalogs: word => dispatch(searchCatalogs(word)),
  searchDocTypes: word => dispatch(searchDocTypes(word)),
  searchDocuments: data => dispatch(searchDocuments(data)),
  resetSearchDocuments: () => dispatch(resetSearchDocuments()),
  deleteDoc: (dirID, docID) => dispatch(deleteDocument(dirID, docID)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, {withTheme: true})(Search));
