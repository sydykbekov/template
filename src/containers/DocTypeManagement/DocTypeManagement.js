import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button} from "@material-ui/core";
import SearchBox from "../../components/SearchBox/SearchBox";
import DataTable from "../../components/DataTable/DataTable";
import {translate} from "../../localization/i18n";
import {deleteDocType, fetchDocTypes, fetchDocTypesNumber} from "../../store/actions/docTypeManagement";
import ConfirmationWindow from "../../components/ConfirmationWindow/ConfirmationWindow";
import {searchDocType} from "../../store/actions/search";
import './DocTypeManagement.css';

class DocTypeManagement extends Component {
  state = {
    isOpen: false,
    id: null,
    found: null,
    currentPage: 1
  };

  componentDidMount() {
    this.props.fetchDocTypesNumber();
    this.props.fetchDocTypes(1);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.lang !== this.props.lang) {
      this.forceUpdate();
    }

    if (prevState.currentPage !== this.state.currentPage) {
      this.props.fetchDocTypesNumber();
      this.props.fetchDocTypes(this.state.currentPage);
    }
  }

  editDocType = id => {
    this.props.history.push(`documents_type/edit/${id}`);
  };

  confirm = id => {
    this.setState({isOpen: true, id});
  };

  deleteType = () => {
    this.props.deleteDocType(this.state.id, this.state.currentPage);
    this.setState({isOpen: false});
  };

  newDocType = () => {
    this.props.history.push('documents_type/new_document_type');
  };

  handleClose = () => {
    this.setState({isOpen: false});
  };

  viewDocType = id => {
    this.props.history.push(`documents_type/view_document_type/${id}`);
  };

  selectChangeHandler = (e) => {
    const word = e.target.value;

    if (word.length >= 3) {
      this.props.searchByWord(word);
    }
  };

  selectTranslation(kz, ru) {
    switch (this.props.lang) {
      case 'KZ_kz':
        return kz;
      default:
        return ru;
    }
  }

  filledHandler = value => {
    const found = [{...value}];
    this.setState({found});
  };

  firstPage = () => {
    this.setState({currentPage: 1});
  };

  lastPage = () => {
    this.setState({currentPage: this.props.docTypesNumber});
  };

  nextPage = () => {
    this.setState({currentPage: this.state.currentPage + 1});
  };

  prevPage = () => {
    this.setState({currentPage: this.state.currentPage - 1});
  };

  render() {
    const {isLoadingNumber, isLoading, foundTypes, searching} = this.props;
    const suggestions = foundTypes.map(type => ({
      label: type.ruName,
      id: type.id,
      kkName: type.kkName,
      ruName: type.ruName
    }));

    return (
      <Fragment>
        <h3>{translate('documentTypeManagement.title')}</h3>
        <Button onClick={this.newDocType} variant="contained"
                color="primary">{translate('documentTypeManagement.button')}</Button>
        <SearchBox placeholder={translate('documentTypeManagement.searchBoxPlaceholder')}
                   array={suggestions}
                   selectChangeHandler={this.selectChangeHandler}
                   filledHandler={this.filledHandler}
                   isLoading={searching}
                   className="top-right_search-box"
                   icon="block"
        />
        <DataTable
          numPages={this.props.docTypesNumber}
          curPage={this.state.currentPage}
          onFirstPage={this.firstPage}
          onLastPage={this.lastPage}
          onNextPage={this.nextPage}
          onPrevPage={this.prevPage}
          isLoading={isLoadingNumber || isLoading}
          rows={(this.state.found || this.props.docTypes)
            .map(type => {
              return {
                id: type.id,
                cols: [
                  {value: () => this.selectTranslation(type.kkName, type.ruName), cb: () => this.viewDocType(type.id)},
                  {value: () => translate('documentTypeManagement.edit'), cb: () => this.editDocType(type.id)},
                  {value: () => translate('documentTypeManagement.delete'), cb: () => this.confirm(type.id)},
                ]
              };
            })
          }
        />
        <ConfirmationWindow isOpen={this.state.isOpen}
                            handleClose={this.handleClose}
                            delete={this.deleteType}/>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  lang: state.localization.language.language,
  docTypesNumber: state.catalogManagement.catalogsNumber,
  docTypes: state.docTypeManagement.docTypes,
  isLoadingNumber: state.docTypeManagement.isLoadingNumber,
  isLoading: state.docTypeManagement.isLoading,
  foundTypes: state.search.result,
  searching: state.search.isLoading
});

const mapDispatchToProps = dispatch => ({
  fetchDocTypesNumber: () => dispatch(fetchDocTypesNumber()),
  fetchDocTypes: page => dispatch(fetchDocTypes(page)),
  deleteDocType: (id, page) => dispatch(deleteDocType(id, page)),
  searchByWord: word => dispatch(searchDocType(word))
});

export default connect(mapStateToProps, mapDispatchToProps)(DocTypeManagement);
