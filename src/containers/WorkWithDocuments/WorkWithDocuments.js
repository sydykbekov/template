import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {translate} from "../../localization/i18n";
import {Button, TextField} from '@material-ui/core';
import AutoComplete from "../../components/AutoComplete/AutoComplete";
import SearchBox from "../../components/SearchBox/SearchBox";
import {fetchCatalogs} from "../../store/actions/catalogManagement";
import {fetchDocTypes} from "../../store/actions/docTypeManagement";
import {searchCatalogs, searchDocTypes} from "../../store/actions/search";
import './WorkWithDocuments.css';
import {deleteDocument, searchDocs} from "../../store/actions/workWithDocs";
import DynamicDataTable from "../../components/DynamicDataTable/DynamicDataTable";
import ConfirmationWindow from "../../components/ConfirmationWindow/ConfirmationWindow";

class WorkWithDocuments extends Component {
  state = {
    directories: [],
    documentTypes: [],
    docName: '',
    dateFrom: '',
    dateTo: '',
    isOpen: false
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.lang !== this.props.lang) {
      this.forceUpdate();
    }
  }

  componentDidMount() {
    this.props.fetchDocTypes(1);
    this.props.fetchCatalogs(1);
  }

  newDocument = () => {
    this.props.history.push('/documents_list/new_document');
  };

  editDocument = (dirID, typeID, id) => {
    this.props.history.push(`/documents_list/${dirID}/type/${typeID}/edit_document/${id}`);
  };

  inputChangeHandler = (key, value) => {
    this.setState({[key]: value});
  };

  onSelect = (key, value) => {
    const data = {
      directories: this.state.directories.map(dir => dir.value),
      documentTypes: this.state.documentTypes.map(type => type.value)
    };
    data[key] = value.map(val => val.value);
    if (data.directories.length > 0 || data.documentTypes.length > 0) {
      this.props.searchDocs(data);
    }
    this.setState({[key]: value});
  };

  onType = (value, event) => {
    if (value.length > 2) {
      switch (event) {
        case 'catalog':
          this.props.searchCatalogs(value);
          break;
        case 'docTypes':
          this.props.searchDocTypes(value);
          break;
        default:
          break;
      }
    }
  };

  viewDocument = (dirID, typeID, id) => {
    this.props.history.push(`/documents_list/${dirID}/type/${typeID}/view_document/${id}`);
  };

  confirm = (dirID, docID) => {
    this.setState({isOpen: true, dirID, docID});
  };

  deleteDoc = () => {
    this.props.deleteDoc(this.state.dirID, this.state.docID);
    this.setState({isOpen: false});
  };

  handleClose = () => {
    this.setState({isOpen: false});
  };

  render() {
    const {foundCatalogs, foundDocTypes, catalogs, docTypes, searchingDocs, foundDocs, lang} = this.props;
    const variant = lang === 'RU_ru' ? 'ruName' : 'kkName';
    let docs = foundDocs.filter(doc => !doc.isDeleted);
    docs = docs.map(doc => ({
      id: doc.id,
      cols: [
        {value: doc.fileName, cb: () => this.viewDocument(doc.directoryId, doc.documentType, doc.id)},
        {value: doc.version},
        {value: doc.directoryId},
        {value: doc.documentType},
        {value: () => translate('workWithDocs.edit'), cb: () => this.editDocument(doc.directoryId, doc.documentType, doc.id)},
        {value: () => translate('workWithDocs.delete'), cb: () => this.confirm(doc.directoryId, doc.id)},
      ]
    }));
    return (
      <Fragment>
        <h3>{translate('workWithDocs.title')}</h3>
        <form className="work-with-docs_search-form">
          <AutoComplete label={translate('workWithDocs.catalog')}
                        placeholder={translate('workWithDocs.catalog')}
                        suggestions={(foundCatalogs.length > 0 ? foundCatalogs : catalogs).map(catalog => ({
                          label: catalog[variant],
                          value: catalog.id
                        }))}
                        onChange={value => this.onSelect('directories', value)}
                        onType={value => this.onType(value, 'catalog')}/>
          <AutoComplete label={translate('workWithDocs.docType')}
                        placeholder={translate('workWithDocs.docType')}
                        suggestions={(foundDocTypes.length > 0 ? foundDocTypes : docTypes).map(type => ({
                          label: type[variant],
                          value: type.id
                        }))}
                        onChange={value => this.onSelect('documentTypes', value)}
                        onType={value => this.onType(value, 'docTypes')}/>
          <SearchBox className="work-with-docs_search"
                     array={[]}
                     selectChangeHandler={event => this.inputChangeHandler('docName', event.target.value)}
                     placeholder={translate('workWithDocs.searchByName')}
                     filledHandler={() => {
                     }}
                     icon="none"/>
          <div className="work-with-docs">
            <TextField
              variant="outlined"
              margin="dense"
              label={translate('search.dateFrom')}
              type="date"
              value={this.state.dateFrom}
              InputLabelProps={{
                shrink: true,
              }}
              onChange={event => this.inputChangeHandler('dateFrom', event.target.value)}
            />
            <TextField
              variant="outlined"
              margin="dense"
              label={translate('search.dateTo')}
              type="date"
              value={this.state.dateTo}
              InputLabelProps={{
                shrink: true,
              }}
              onChange={event => this.inputChangeHandler('dateTo', event.target.value)}
            />
          </div>
        </form>
        <Button variant="contained" color="primary" style={{display: 'block', clear: 'both'}}
                onClick={this.newDocument}>{translate('workWithDocs.button')}</Button>
        <DynamicDataTable
          isLoading={searchingDocs}
          header={[
            {id: "filename", label: translate('workWithDocs.colFilename')},
            {id: "date_version", label: translate('workWithDocs.colDateVer')},
            {id: "catalog", label: translate('workWithDocs.colCatalog')},
            {id: "doc_type", label: translate('workWithDocs.colDocType')},
            {id: null},
            {id: null}
          ]}
          rows={docs}
          orderBy='filename'/>
        <ConfirmationWindow isOpen={this.state.isOpen}
                            handleClose={this.handleClose}
                            delete={this.deleteDoc} />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  lang: state.localization.language.language,
  foundCatalogs: state.search.foundCatalogs,
  foundDocTypes: state.search.foundDocTypes,
  catalogs: state.catalogManagement.catalogs,
  docTypes: state.docTypeManagement.docTypes,
  searchingDocs: state.workWithDocs.searchingDocs,
  foundDocs: state.workWithDocs.foundDocs
});

const mapDispatchToProps = dispatch => ({
  fetchCatalogs: page => dispatch(fetchCatalogs(page)),
  fetchDocTypes: page => dispatch(fetchDocTypes(page)),
  searchCatalogs: word => dispatch(searchCatalogs(word)),
  searchDocTypes: word => dispatch(searchDocTypes(word)),
  searchDocs: data => dispatch(searchDocs(data)),
  deleteDoc: (dirID, docID) => dispatch(deleteDocument(dirID, docID))
});

export default connect(mapStateToProps, mapDispatchToProps)(WorkWithDocuments);
