import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'react-router-redux';
import store, {history} from './store/configureStore';
import axios from './axios-api';

axios.interceptors.request.use(config => {
  try {
    config.headers['Authorization'] = 'Bearer ew0KIGFsZzogIkhTNTEyIg0KfS4NCnsNCiBzdWI6ICI5MzA2MDYzNTEwNDIiLA0KIGl0ZW1zOiBbDQogICJDQU5fUkVBRF9GSUxFU19ESVJFQ1RPUlkiLA0KICAiQ0FOX1JFQURfRE9DX1RZUEVTIiwNCiAgIkNBTl9NQU5BR0VfRElSRUNUT1JZXzMiLA0KICAiQ0FOX0NSRUFURV9ESVJFQ1RPUlkiDQogXSwNCiBleHA6IDE1NDA0NjczODENCn0=';
  } catch (e) {
    // do nothing
  }
  return config;
});

const app = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

