import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://212.2.230.94:10190/'
});

export default instance;